/**
 * 
 */
package com.cencor.devtools.javacore.service;

import java.sql.Connection;
import java.sql.SQLException;

import com.cencor.devtools.javacore.entity.Person;
import com.cencor.devtools.javacore.repository.ConnectionManager;
import com.cencor.devtools.javacore.repository.EntityAccessException;
import com.cencor.devtools.javacore.repository.PersonRepository;
import com.cencor.devtools.javacore.repository.tx.TransactionManager;

/**
 * @author gus
 *
 */
public class TransactionalPersonService implements PersonService {

    /***/
    private final TransactionManager txManager;
    /***/
    private final PersonRepository repository;
    /***/
    private final ConnectionManager connManager;
    
    public TransactionalPersonService(final TransactionManager txManager, final PersonRepository repository,
            final ConnectionManager connManager) {
        this.txManager = txManager;
        this.repository = repository;
        this.connManager = connManager;
    }
    
    @Override
    public Person save(final Person person) {
        try (Connection conn = connManager.getConnection()) {
            return transactionalSave(person, conn);
        } catch (SQLException e) {
            throw new EntityAccessException("Ha ocurrido un error en la conexion a la BD", e);
        }
    }

    private Person transactionalSave(final Person person, final Connection conn) {
        Person actual = null;
        try {
            txManager.start(conn);
            
            actual = repository.save(conn, person);
            /*
             * Pudieran haber multiples guardados o modificaciones que serian parte de la
             * misma tx
             */
            
            txManager.commit(conn);
        } catch (SQLException e) {
            txManager.rollback(conn);
            throw new EntityAccessException("Ha ocurrido un error durante la transaccion", e);
        }
        return actual;
    }

}
