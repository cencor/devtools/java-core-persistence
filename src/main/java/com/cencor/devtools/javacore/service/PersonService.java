/**
 * 
 */
package com.cencor.devtools.javacore.service;

import com.cencor.devtools.javacore.entity.Person;

/**
 * @author gus
 *
 */
public interface PersonService {

    Person save(Person person);
}
