/**
 * 
 */
package com.cencor.devtools.javacore.entity;

/**
 * @author gus
 *
 */
public class Person {

    /***/
    private Integer id;
    /***/
    private String name;
    /***/
    private String lastName;
    /***/
    private String secondLastName;
    
    public Integer getId() {
        return id;
    }
    public void setId(final Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(final String name) {
        this.name = name;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }
    public String getSecondLastName() {
        return secondLastName;
    }
    public void setSecondLastName(final String secondLastName) {
        this.secondLastName = secondLastName;
    }
}
