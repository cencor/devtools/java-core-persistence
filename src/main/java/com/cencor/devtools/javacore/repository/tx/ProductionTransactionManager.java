/**
 * 
 */
package com.cencor.devtools.javacore.repository.tx;

import java.sql.Connection;
import java.sql.SQLException;

import com.cencor.devtools.javacore.repository.EntityAccessException;

/**
 * @author gus
 *
 */
public class ProductionTransactionManager implements TransactionManager {

    @Override
    public void start(final Connection conn) {
        try {
            conn.setAutoCommit(false);
        } catch (SQLException e) {
            throw new EntityAccessException("No se ha podido iniciar una transaccion", e);
        }
    }

    @Override
    public void commit(final Connection conn) {
        try {
            conn.commit();
            conn.setAutoCommit(true);
        } catch (SQLException e) {
            throw new EntityAccessException("No se ha podido confirmar (commit) la transaccion", e);
        }
    }

    @Override
    public void rollback(final Connection conn) {
        try {
            conn.rollback();
            conn.setAutoCommit(true);
        } catch (SQLException e) {
            throw new EntityAccessException("No se ha podido dar rollback a la transaccion", e);
        }
    }

}
