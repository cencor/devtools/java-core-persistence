/**
 * 
 */
package com.cencor.devtools.javacore.repository.tx;

import java.sql.Connection;

/**
 * @author gus
 *
 */
public interface TransactionManager {

    void start(Connection conn);
    
    void commit(Connection conn);
    
    void rollback(Connection conn);
}
