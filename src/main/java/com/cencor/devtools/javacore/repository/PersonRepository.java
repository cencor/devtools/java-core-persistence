/**
 * 
 */
package com.cencor.devtools.javacore.repository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;

import com.cencor.devtools.javacore.entity.Person;

/**
 * @author gus
 *
 */
public interface PersonRepository {

    Person save(Connection conn, Person person) throws SQLException;
    
    Optional<Person> findById(Connection conn, int id) throws SQLException;
}
