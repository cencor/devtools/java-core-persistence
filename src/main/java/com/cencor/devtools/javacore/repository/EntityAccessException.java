/**
 * 
 */
package com.cencor.devtools.javacore.repository;

/**
 * @author gus
 *
 */
public class EntityAccessException extends RuntimeException {

    /***/
    private static final long serialVersionUID = -501764101231092901L;

    public EntityAccessException(final String desc, final Throwable cause) {
        super(desc, cause);
    }
}
