/**
 * 
 */
package com.cencor.devtools.javacore.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author gus
 *
 */
public class ConnectionManager {

    /***/
    private final String url;
    /***/
    private final String username;
    /***/
    private final String password;
    
    public ConnectionManager(final String url, final String username, final String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }
    
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, username, password);
    }
}
