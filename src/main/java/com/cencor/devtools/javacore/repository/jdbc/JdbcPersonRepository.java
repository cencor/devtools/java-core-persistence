/**
 * 
 */
package com.cencor.devtools.javacore.repository.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;

import com.cencor.devtools.javacore.entity.Person;
import com.cencor.devtools.javacore.repository.PersonRepository;

/**
 * @author gus
 *
 */
public class JdbcPersonRepository implements PersonRepository {

    @Override
    public Person save(final Connection conn, final Person person) throws SQLException {
        String insert = "INSERT INTO Person(name, last_name, second_last_name) VALUES (?, ?, ?)";
        try (PreparedStatement pstmt = conn.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS)) {
            int index = 1;
            pstmt.setString(index++, person.getName());
            pstmt.setString(index++, person.getLastName());
            pstmt.setString(index++, person.getSecondLastName());
            
            pstmt.executeUpdate();
            
            ResultSet generatedIdRs = pstmt.getGeneratedKeys();
            if (generatedIdRs.next()) {
                person.setId(generatedIdRs.getInt(1));
            }
        }
        return person;
    }

    @Override
    public Optional<Person> findById(final Connection conn, final int id) throws SQLException {
        String select = "SELECT id, name, last_name, second_last_name FROM Person WHERE id = ?";
        Person actual = null;
        try (PreparedStatement pstmt = conn.prepareStatement(select)) {
            pstmt.setInt(1, id);
            
            try (ResultSet rs = pstmt.executeQuery()) {
                if (rs.next()) {
                    int index = 1;
                    actual = new Person();
                    actual.setId(rs.getInt(index++));
                    actual.setName(rs.getString(index++));
                    actual.setLastName(rs.getString(index++));
                    actual.setSecondLastName(rs.getString(index++));
                }
            }
        }
        return Optional.ofNullable(actual);
    }

}
