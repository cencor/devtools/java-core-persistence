/**
 * 
 */
package com.cencor.devtools.javacore.service;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.cencor.InMemoryDbConfig;
import com.cencor.devtools.javacore.entity.Person;
import com.cencor.devtools.javacore.repository.ConnectionManager;
import com.cencor.devtools.javacore.repository.EntityAccessException;
import com.cencor.devtools.javacore.repository.PersonRepository;
import com.cencor.devtools.javacore.repository.jdbc.JdbcPersonRepository;
import com.cencor.devtools.javacore.repository.tx.TestTransactionManager;
import com.cencor.devtools.javacore.repository.tx.TransactionManager;

/**
 * @author gus
 * Levanta Spring para poder crear la BD en memoria.
 */
@SpringBootTest(classes = InMemoryDbConfig.class)
class TransactionalPersonServiceTest {

    /***/
    private TransactionManager txManager;
    /***/
    private PersonRepository repository;
    /***/
    private ConnectionManager connManager;
    /***/
    private PersonService service;
    
    @BeforeEach
    void setup() {
        String url = "jdbc:hsqldb:mem:testdb;DB_CLOSE_DELAY=-1";
        String user = "sa";
        String password = "";
        this.connManager = new ConnectionManager(url, user, password);
        this.repository = new JdbcPersonRepository();
        this.txManager = new TestTransactionManager();
        
        this.service = new TransactionalPersonService(txManager, repository, connManager);
    }
    
    @Test
    void savePerson() {
        Person gus = new Person();
        gus.setName("Gus");
        gus.setLastName("Vargas");
        gus.setSecondLastName("Tena");
        
        Person actual = this.service.save(gus);
        
        Assertions.assertNotNull(actual.getId());
        Assertions.assertEquals(gus.getName(), actual.getName());
        Assertions.assertEquals(gus.getLastName(), actual.getLastName());
        Assertions.assertEquals(gus.getSecondLastName(), actual.getSecondLastName());
    }
    
    @Test
    void saveIncompletePersonRollsbackTx() {
        Person gus = new Person();
        gus.setName("Gus");
        gus.setSecondLastName("Tena");
        
        // Falla pues el apellido es obligatorio.
        Assertions.assertThrows(EntityAccessException.class, () -> this.service.save(gus));
    }
    
    @Test
    void cantGetDbConnection() throws SQLException {
        this.connManager = Mockito.mock(ConnectionManager.class);
        Mockito.when(connManager.getConnection()).thenThrow(SQLException.class);
        
        this.service = new TransactionalPersonService(txManager, repository, connManager);
        
        Person p = new Person();
        Assertions.assertThrows(EntityAccessException.class, () -> this.service.save(p));
    }
    
    @Test
    void verifyRollbackOnException() throws SQLException {
        this.connManager = Mockito.mock(ConnectionManager.class);
        Connection mockedConn = Mockito.mock(Connection.class);
        this.txManager = Mockito.mock(TransactionManager.class);
        this.repository = Mockito.mock(PersonRepository.class);
        Person p = new Person();
        this.service = new TransactionalPersonService(txManager, repository, connManager);
        
        Mockito.when(connManager.getConnection()).thenReturn(mockedConn);
        Mockito.when(repository.save(mockedConn, p)).thenThrow(SQLException.class);
        
        Assertions.assertThrows(EntityAccessException.class, () -> this.service.save(p));
        
        Mockito.verify(txManager).start(mockedConn);
        Mockito.verify(txManager).rollback(mockedConn);
    }
}
