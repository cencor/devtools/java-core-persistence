/**
 * 
 */
package com.cencor.devtools.javacore.repository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.cencor.InMemoryDbConfig;
import com.cencor.devtools.javacore.entity.Person;
import com.cencor.devtools.javacore.repository.jdbc.JdbcPersonRepository;
import com.cencor.devtools.javacore.repository.tx.TestTransactionManager;
import com.cencor.devtools.javacore.repository.tx.TransactionManager;

/**
 * @author gus
 * Utilizamos Spring unicamente para crear la BD en memoria con liquibase.
 */
@SpringBootTest(classes = InMemoryDbConfig.class)
class PersonRepositoryTest {

    /***/
    private ConnectionManager connManager;
    /***/
    private PersonRepository repository;
    
    @BeforeEach
    void setup() {
        String url = "jdbc:hsqldb:mem:testdb;DB_CLOSE_DELAY=-1";
        String user = "sa";
        String password = "";
        this.connManager = new ConnectionManager(url, user, password);
        this.repository = new JdbcPersonRepository();
    }
    
    @Test
    void savePerson() throws SQLException {
        Person gus = new Person();
        gus.setName("Gustavo");
        gus.setLastName("Vargas");
        gus.setSecondLastName("Tena");
        Optional<Person> personFoundById = Optional.empty();
        
        TransactionManager txManager = new TestTransactionManager();
        try (Connection conn = connManager.getConnection()) {
            txManager.start(conn);
            gus = this.repository.save(conn, gus);
            
            personFoundById = this.repository.findById(conn, gus.getId());
            txManager.rollback(conn);
        }
        Assertions.assertTrue(personFoundById.isPresent());
        Person actual = personFoundById.get();
        Assertions.assertEquals(gus.getId(), actual.getId());
        Assertions.assertEquals(gus.getName(), actual.getName());
        Assertions.assertEquals(gus.getLastName(), actual.getLastName());
    }
    
    @Test
    void findByNonPersistedId() throws SQLException {
        Optional<Person> personFoundById = Optional.empty();
        try (Connection conn = connManager.getConnection()) {
            personFoundById = this.repository.findById(conn, Integer.MIN_VALUE);
        }
        Assertions.assertTrue(personFoundById.isEmpty());
    }
}
