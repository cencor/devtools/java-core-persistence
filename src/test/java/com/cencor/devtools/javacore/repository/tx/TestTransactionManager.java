/**
 * 
 */
package com.cencor.devtools.javacore.repository.tx;

import java.sql.Connection;
import java.sql.SQLException;

import com.cencor.devtools.javacore.repository.EntityAccessException;

/**
 * @author gus
 *
 */
public class TestTransactionManager implements TransactionManager {

    @Override
    public void start(final Connection conn) {
        try {
            conn.setAutoCommit(false);
        } catch (SQLException e) {
            throw new EntityAccessException(null, e);
        }
    }

    @Override
    public void commit(final Connection conn) {
        // En pruebas siempre rollback para no afectar el estado inicial de la BD en memoria
        try {
            conn.rollback();
            conn.setAutoCommit(true);
        } catch (SQLException e) {
            throw new EntityAccessException(null, e);
        }
    }

    @Override
    public void rollback(final Connection conn) {
        try {
            conn.rollback();
            conn.setAutoCommit(true);
        } catch (SQLException e) {
            throw new EntityAccessException(null, e);
        }
    }

}
