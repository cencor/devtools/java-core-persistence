package com.cencor.devtools.javacore.repository.tx;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.cencor.devtools.javacore.repository.EntityAccessException;

@ExtendWith(MockitoExtension.class)
class ProductionTransactionManagerTest {

    /***/
    @Mock
    private Connection conn;
    /***/
    private TransactionManager txManager;
    
    @BeforeEach
    void setup() {
        txManager = new ProductionTransactionManager();
    }
    
    @Test
    void start() throws SQLException {
        txManager.start(conn);
        
        Mockito.verify(conn).setAutoCommit(false);
    }
    
    @Test
    void cantTurnOffAutoCommit() throws SQLException {
        Mockito.doThrow(SQLException.class).when(conn).setAutoCommit(false);
        
        Assertions.assertThrows(EntityAccessException.class, () -> txManager.start(conn));
        
    }
    
    @Test
    void commit() throws SQLException {
        txManager.commit(conn);
        
        Mockito.verify(conn).commit();
        Mockito.verify(conn).setAutoCommit(true);
    }
    
    @Test
    void cantCommit() throws SQLException {
        Mockito.doThrow(SQLException.class).when(conn).commit();
        
        Assertions.assertThrows(EntityAccessException.class, () -> txManager.commit(conn));
        
    }
    
    @Test
    void rollback() throws SQLException {
        txManager.rollback(conn);
        
        Mockito.verify(conn).rollback();
        Mockito.verify(conn).setAutoCommit(true);
    }
    
    @Test
    void cantRollback() throws SQLException {
        Mockito.doThrow(SQLException.class).when(conn).rollback();
        
        Assertions.assertThrows(EntityAccessException.class, () -> txManager.rollback(conn));
        
    }
    
}
